/* global L, distance */

var pot;

// seznam z markerji na mapi
var markerji = [];

var mapa;


window.addEventListener('load', function () {
  
  // Osnovne lastnosti mape
  var mapOptions = {
    center: [46.06612849282597, 14.6246379315877],
    zoom: 8
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  pridobiPodatke(function(jsonRezultat){
    podatkiOBolnisnicah(jsonRezultat, function (out) {
      izrisRezultatov(out);
    })
  });


  // Objekt oblačka markerja
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    popup
        .setLatLng(latlng)
        .setContent("Izbrana točka:" + latlng.toString())
        .openOn(mapa);

    posodobiOznakeNaZemljevidu(e.latlng);
  }

  mapa.on('click', obKlikuNaMapo);
  
  var radij = 5;
  var lokacija;

  mapa.on("click", function(e){
    lokacija = e;
    obKlikuNaMapo(e);
  });

});

/**
 * Za podano vrsto interesne točke dostopaj do JSON datoteke
 * in vsebino JSON datoteke vrni v povratnem klicu
 *
 * @param vrstaInteresneTocke "fakultete" ali "restavracije"
 * @param callback povratni klic z vsebino zahtevane JSON datoteke
 */
function pridobiPodatke(callback) {

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
      var json = JSON.parse(xobj.responseText);
      callback(json);
    }
  };
  xobj.send(null);
}


function dodajPoligon(tocke, naziv, naslov){

  var poligon = L.polygon(tocke);

  poligon.addTo(mapa);
  if(naziv !== undefined && naslov !== undefined) poligon.bindPopup("<div>"+naziv+"<br>"+naslov+"</div");
  else poligon.bindPopup("<div>Ni podatkov</div>")

  markerji.push(poligon);

  //if(poligon !== 'undefined') poligon.addTo(mapa);
}

function podatkiOBolnisnicah(podatki, callback){

  var podatki1 = podatki.features;

  for(var i = 0; i < podatki1.length; i++){
    var bolnisnica = podatki1[i].geometry.coordinates;

    if(podatki1[i].geometry.type === "Polygon"){
      for(var j=0; j < bolnisnica.length; j++){
        for(var k=0; k < bolnisnica[j].length; k++){
          var spremenljivka1 = bolnisnica[j][k][0];
          bolnisnica[j][k][0] = bolnisnica[j][k][1];
          bolnisnica[j][k][1] = spremenljivka1;
        }
      }
    }
    else if(podatki1[i].geometry.type === "LineString"){
      for(var l=0; l < bolnisnica.length; l++){
        var spremenljivka2 = bolnisnica[l][0];
        bolnisnica[l][0] = bolnisnica[l][1];
        bolnisnica[l][1] = spremenljivka2;
      }
    }
  }

  callback(podatki);
}


/**
 * Na podlagi podanih interesnih točk v GeoJSON obliki izriši
 * posamezne točke na zemljevid
 *
 * @param jsonRezultat interesne točke v GeoJSON obliki
 */
function izrisRezultatov(jsonRezultat) {
  var znacilnosti = jsonRezultat.features;

  for (var i = 0; i < znacilnosti.length; i++) {
    var podatki2 = znacilnosti[i].properties;
    var naziv;
    var naslov;
    if (podatki2["name"] !== undefined) naziv = podatki2["name"];
    if(podatki2["addr:housenumber"] !== undefined) naslov = podatki2["addr:street"]+podatki2["addr:housenumber"]+", "+podatki2["addr:city"];

    if (podatki2["type"] === 'multipolygon'){
      var tocke1 = znacilnosti[i].geometry.coordinates[0];
      var tocke2 = znacilnosti[i].geometry.coordinates[1];
      dodajPoligon(tocke1, name, naslov);
      dodajPoligon(tocke2, name, naslov);

    } else if (znacilnosti[i].geometry.type !== "LineString"){
      var tocke3 = znacilnosti[i].geometry.coordinates;
      dodajPoligon(tocke3, naziv, naslov);

    }else{
      var tocke4 = znacilnosti[i].geometry.coordinates;
      dodajPoligon(tocke4, naziv, naslov);
    }
  }
}


/**
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param location je trenutna lokacija
 */
function prikaziOznako(lng, lat, location) {
  const r = 1;
  return distance(lat, lng, location.lat, location.lng, "K") < r;
}

function posodobiOznakeNaZemljevidu(location) {
  for (var i=1; i < markerji.length; i++) {
    if(markerji[i]._latlngs !== undefined ) {
      var lat = markerji[i]._latlngs[0][0].lat;
      var lng = markerji[i]._latlngs[0][0].lng;
      markerji[i].setStyle({
        color: prikaziOznako(lng, lat, location) ? 'green' : 'blue'
      });
    }
  }
}


