/* global ApexCharts */
var data013 = {
            chart: {
                width: 700,
                type: 'donut',
            },
            dataLabels: {
                enabled: true
            },
            series: [0.6, 45, 0.015, 11, 0.06, 0.9, 0.9, 12, 1, 0.3, 0.0018, 4],
            labels: ["Vitamin A: 0,6 mg/dan","Vitamin C: 45 mg/dan","Vitamin D: 0,015 mg/dan", "Vitamin E: 11 mg/dan", "Vitamin K:0,06 mg/dan", "Vitamin B1: 0,9 mg/dan", "Vitamin B2: 0,9 mg/dan", "Vitamin B3: 12 mg/dan", "Vitamin B6: 1 mg/dan", "Vitamin B9: 0,3 mg/dan", "B12: 0,0018 mg/dan","Vitamin B5: 4 mg/dan"],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        show: false
                    }
                }
            }],
            legend: {
                verticalAlign: 'right',
                position: 'right',
                offsetY: 0,
                height: 300,
            }
        }
        

        
var data1418 = {
            chart: {
                width: 700,
                type: 'donut',
            },
            dataLabels: {
                enabled: true
            },
            series: [0.7, 65, 0.015, 15, 0.075, 1, 1, 14, 1.2, 0.4, 0.0024, 5],
            labels: ["Vitamin A: 0,7 mg/dan","Vitamin C: 65 mg/dan","Vitamin D: 0,015 mg/dan", "Vitamin E: 15 mg/dan", "Vitamin K:0,075 mg/dan", "Vitamin B1: 1 mg/dan", "Vitamin B2: 1 mg/dan", "Vitamin B3: 14 mg/dan", "Vitamin B6: 1.2 mg/dan", "Vitamin B9: 0,4 mg/dan", "Vitamin B12: 0,0024 mg/dan","Vitamin B5: 5 mg/dan"],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        show: false
                    }
                }
            }],
            legend: {
                verticalAlign: 'right',
                position: 'right',
                offsetY: 0,
                height: 300,
            }
        }
        
var data1950 = {
            chart: {
                width: 700,
                type: 'donut',
            },
            dataLabels: {
                enabled: true
            },
            series: [0.9, 90, 0.015, 15, 0.12, 1.2, 1.3, 16, 1.3, 0.4, 0.0024, 5],
            labels: ["Vitamin A: 0,9 mg/dan","Vitamin C: 90 mg/dan","Vitamin D: 0,015 mg/dan", "Vitamin E: 15 mg/dan", "Vitamin K:0,12 mg/dan", "Vitamin B1: 1,2 mg/dan", "Vitamin B2: 1,3 mg/dan", "Vitamin B3: 16 mg/dan", "Vitamin B6: 1,3 mg/dan", "Vitamin B9: 0,4 mg/dan", "Vitamin B12: 0,0024 mg/dan","Vitamin B5: 5 mg/dan"],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        show: false
                    }
                }
            }],
            legend: {
                verticalAlign: 'right',
                position: 'right',
                offsetY: 0,
                height: 300,
            }
        }
        
var data5170 = {
            chart: {
                width: 700,
                type: 'donut',
            },
            dataLabels: {
                enabled: true
            },
            series: [0.7, 75, 0.015, 15, 0.09, 1.1, 1.1, 14, 1.5, 0.4, 0.0024, 5],
            labels: ["Vitamin A: 0,7 mg/dan","Vitamin C: 75 mg/dan","Vitamin D: 0,015 mg/dan", "Vitamin E: 15 mg/dan", "Vitamin K:0,09 mg/dan", "Vitamin B1: 1,1 mg/dan", "Vitamin B2: 1,1 mg/dan", "Vitamin B3: 14 mg/dan", "Vitamin B6: 1,5 mg/dan", "Vitamin B9: 0,4 mg/dan", "Vitamin B12: 0,0024 mg/dan","Vitamin B5: 5 mg/dan"],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        show: false
                    }
                }
            }],
            legend: {
                verticalAlign: 'right',
                position: 'right',
                offsetY: 0,
                height: 300,
            }
        }

var data70 = {
            chart: {
                width: 700,
                type: 'donut',
            },
            dataLabels: {
                enabled: true
            },
            series: [0.9, 90, 0.002, 15, 0.12, 1.2, 1.3, 16, 1.7, 0.4, 0.0024, 5],
            labels: ["Vitamin A: 0,9 mg/dan","Vitamin C: 90 mg/dan","Vitamin D: 0,002 mg/dan", "Vitamin E: 15 mg/dan", "Vitamin K:0,12 mg/dan", "Vitamin B1: 1,2 mg/dan", "Vitamin B2: 1,3 mg/dan", "Vitamin B3: 16 mg/dan", "Vitamin B6: 1,7 mg/dan", "Vitamin B9: 0,4 mg/dan", "Vitamin B12: 0,0024 mg/dan","Vitamin B5: 5 mg/dan"],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        show: false
                    }
                }
            }],
            legend: {
                verticalAlign: 'right',
                position: 'right',
                offsetY: 0,
                height: 300,
            }
        }

 window.addEventListener('load', function() {
     
    document.getElementById("preveriPodatke").addEventListener("click", function() {
        
        var chart = new ApexCharts(
            document.querySelector("#diagram"),
            data013
        );

        chart.render()
    
    });  

})
