/*global $*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


window.addEventListener('load', function () {
  
  
    document.getElementById('vitaminA').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'Vitamin A je pomemben za normalno rast, za delovanje imunskega sistema, delovanje vida, presnovo železa, za izgradnjo kože in sluznic in njihovo delovanje, nudi oporo pri reprodukciji in vpliva na razvoj celic in tkiv. Dobimo ga v ribjem olju, jetrih, jajčnih rumenjakih, maslu, smetani.';
  });
  
    document.getElementById('vitaminC').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'Vitamin C ima v telesu pomembno vlogo pri delovanju imunskega sistema, sodeluje v procesu tvorbe kolagena, ki potreben za normalno delovanje žil, kosti, hrustanca, dlesni, kože in zob. Dobimo ga v črnem ribezu, rdeči papriki, brstičnem ohrovtu, Jagodah, kuhan brokoliju.';
  });
  
     document.getElementById('vitaminD').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'Glavna fiziološka funkcija vitamina D je, da poveča zmožnost tankega črevesa, da absorbira kalcij in fosfor, ter s tem pomaga vzdrževati ustrezno koncentracijo kalcija in fosforja v krvi. Dobimo ga v ribjem olju, lososu, sardinah, jajcih, margarini, tuni.';
  });
  
     document.getElementById('vitaminE').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'Osnovna vloga vitamin E je njegovo antioksidativno delovanje. Dobimo ga v pšeničnih kalčkih, zelju, solati, zeleni, koruzi, soji.';
  });
  
     document.getElementById('vitaminK').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'Vitamin K ima v telesu funkcijo aktivacije beljakovin, ki so potrebne za normalno strjevanje krvi, preprečuje kalcificiranje arterij in drugih mehkih tkiv. Dobimo ga v špinači, zelju, brokoliju, jajcih, telečjih jetrih.';
  });
  
     document.getElementById('vitaminB1').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'B1 ima pomembno vlogo pri delovanju živčevja, natančneje pri prevajanju živčnih signalov med celicami živčevja. Dobimo ga v suhem pivskem kvasu, kuhanem krompirju, tuni, rdečem fižolu, svinjini.';
  });
  
     document.getElementById('vitaminB2').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'B2 prispeva k normalnemu metabolizmu pridobivanja energije, dobremu delovanju živčnega sistema, vida in kože. Dobimo ga v mleku, brancinu, zeleni solati, špinači, svinjini, polnozrnati pšenični moki.';
  });
  
     document.getElementById('vitaminB3').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'V telesu je B3 prekurzor dveh molekul, ki imata velik pomen pri številnih kemijskih reakcijah, ki potekajo v telesu. Dobimo ga v sardinah, telečjih jetrih, piščančjih prsih, pivskem kvasu, rdeči papriki.';
  });
  
     document.getElementById('vitaminB6').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'B6 v telesu sodeluje v več kot sto reakcijah in ima osrednjo funkcijo pri metabolizmu aminokislin, uravnavanju hormonov in mnogih drugih. Dobimo ga v bananah, piščancu, leči, krompirju, lososu, brokoliju, špinači.';
  });
  
     document.getElementById('vitaminB9').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'B9 je potreben za normalno psihološko delovanje, normalno delovanje imunskega sistema in za zmanjševanje utrujenosti in izčrpanosti. Dobimo ga v govejih jetrih, čičeriki, črnem fižolu, soji, leči, beluših.';
  });
  
     document.getElementById('vitaminB12').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'B12 je potreben za normalno delovanje imunskega in živčnega sistema, za normalno psihološko delovanje, sodeluje pri presnovi hranil, sintezi DNK in pri presnovi homocisteina. Dobimo ga v jetrih, školjkah, tuni, lososu, jajcih, siru, šunki.';
  });
  
     document.getElementById('vitaminB5').addEventListener('click', function() {
    document.getElementById('izpis').innerHTML = 'B5 ima v telesu ključno vlogo pri presnovnih procesih pridobivanja energije. Dobimo ga v telečjih jetrih, kuhanih piščančjih prsih, grahu, avokadu, jajcih.';
  });
  


});


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  
}

function kreirajEHRzaBolnika() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();
  var visina = $("#kreirajVisino").val();
  var teza = $("#kreirajTezo").val();

	if (!ime || !priimek || !datumRojstva || !visina || !teza ||
	    ime.trim().length == 0 || priimek.trim().length == 0 || datumRojstva.trim().length == 0 ||
	    visina.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          height: visina,
          weight: teza,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
	}
}
function dodajMeritve(ehrId, telesnaVisina, telesnaTeza) {
    var compositionData = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    };
    var queryParams = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT'
        //committer: 'Belinda Nurse'
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(queryParams),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(compositionData),
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (res) {
            $("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-success fade-in'>" +
                result.meta.href + ".</span>");
        },
        error: function(err) {
            $("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(error.responseText).userMessage + "'!");
        }
    });

}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Bella Ramsey).
   */
  $('#preberiPredlogoUporabnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajVisino").val(podatki[3]);
    $("#kreirajTezo").val(podatki[4]);
    
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});


});


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
